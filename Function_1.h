#include "windows.h"
void printnumbs()
{
	const int a = 20;
	int num = 0;
	int number = 0;
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	while (num > 2 || num < 1)
	{ 
		SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
		std::cout << "Please Enter the number 1 for output odd numbers or Enter the number 2 for output even numbers: ";
		std::cin >> num;
		std::cout << "\n";
	}
	
	switch (num)
	{
		case 1: //num == 1
			for (int i = 0; i <= a; i++)
			{
				int mod = i % 2;
				if (mod != 0)
				{
					number++;
					std::cout << number << ") " << i << "\n";
				}

			}
		break;

		case 2: //num == 2
			for (int i = 0; i <= a; i++)
			{
				int mod = i % 2;

				if (mod == 0)
				{
					number++;
					std::cout << number << ") " << i << "\n";
				}

			}
		break;
	}
}
